﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace лаба10
{
    class Plane
    {
        public string type;
        public int[] cost;

        public Plane(string type, int[] cost)
        {
            this.type = type;
            this.cost = cost;
        }

        public static Plane[] ReadFromFile()
        {
            StreamReader f = new StreamReader("text1.txt", Encoding.Default);
            String s = f.ReadToEnd();
            f.Close();

            string[] lines = s.Split('\n');
            Plane[] planes = new Plane[lines.Length];
            for (int i = 0; i < lines.Length; i++)
            {
                string[] data = lines[i].Split(';');
                string type = data[0];
                int cost1 = Convert.ToInt32(data[1]);
                int cost2 = Convert.ToInt32(data[2]);
                int cost3 = Convert.ToInt32(data[3]);
                planes[i] = new Plane(type, new int[] {cost1, cost2, cost3});
            }
            return planes;
        }


        public static Plane GetPlaneByType(Plane[] planes, string type)
        {
            for (int i = 0; i < planes.Length; i++)
            {
                if (planes[i].type == type)
                    return planes[i];
            }
            return null;
        }

        private event MyDelegate myEvent;

        public void Reg(MyDelegate d)
        {
            myEvent += d;
        }

        public void ChangeCost(int[] newCost)
        {
            cost = newCost;
            if (myEvent != null) myEvent(this);
        }

        public void IncreaseCostOn15()
        {
            for (int i = 0; i < cost.Length; i++ )
                cost[i] = cost[i] + (int)(cost[i] * 0.15);
            if (myEvent != null) myEvent(this);
        }

    }
}
