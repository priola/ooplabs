﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace лаба10
{
    delegate void MyDelegate(object o);

    class Program
    {
        static void Main(string[] args)
        {

            Flight[] voyage = Flight.ReadFromFile();
            Plane[] planes = Plane.ReadFromFile();

            Flight.Info("Initial data", voyage, planes);

            for (int i = 0; i < voyage.Length; i++)
            {
                Plane plane = Plane.GetPlaneByType(planes, voyage[i].type);
                plane.Reg(new MyDelegate(voyage[i].ChangeCost));
            }
            Console.WriteLine("enter new cost");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            int c = Convert.ToInt32(Console.ReadLine());

            planes[0].ChangeCost(new int[] {a, b, c});

            Flight.Info("After cost changing", voyage, planes);

            planes[0].IncreaseCostOn15();

            Flight.Info("After cost increasing", voyage, planes);

            Console.ReadKey();
        }
    }
}
