﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace лаба10
{
    class Flight
    {
        public int flightNumber;
        public string type;
        public string destination;
        public string date;
        public string time;
        public int[] cost;

        public Flight() {}

        public Flight(int flightNumber, string type, string destination, string date, string time, int[] cost)
        {
            this.flightNumber = flightNumber;
            this.type = type;
            this.destination = destination;
            this.date = date;
            this.time = time;
            this.cost = cost;
        }

        public static Flight[] ReadFromFile()
        {
            StreamReader f = new StreamReader("text.txt", Encoding.Default);
            String s = f.ReadToEnd();
            f.Close();

            Plane[] planeArray = Plane.ReadFromFile();

            string[] lines = s.Split('\n');
            Flight[] voyage = new Flight[lines.Length];

            for (int i = 0; i < lines.Length; i++)
            {
                string[] data = lines[i].Split(';');
                int flightNumber = Convert.ToInt32(data[0]);
                string type = data[1];
                string destination = data[2];
                string date = data[3];
                string time = data[4];
                Plane plane = Plane.GetPlaneByType(planeArray, type);
                voyage[i] = new Flight(flightNumber, type, destination , date, time, plane.cost);
            }
            return voyage;
        }

        public static void Info(string text, Flight[] voyage, Plane[] planes)
        {
            string line = "==============================================================";
            string header = "| № |    Type    |  Date  |  Time  |___________Cost___________|";
            string header1 = "|   |            |        |        | economy| business| first |";
            string data = "|{0,-3}|{1,-12}|{2,-8}|{3,-8}|{4,-8}|{5,-9}|{6,-7}";

            Console.WriteLine("\n\n" + text + "\n");

            List<string> place = Flight.GetPlace(voyage);
            for (int j = 0; j < place.Count; j++)
            {
                Console.WriteLine(place[j]);
                Console.WriteLine(line);
                Console.WriteLine(header);
                Console.WriteLine(header1);
                Console.WriteLine(line);

                List<Flight> flightList = new List<Flight>();

                for (int i = 0; i < voyage.Length; i++)
                {
                    if (place[j] == voyage[i].destination)
                        flightList.Add(voyage[i]);
                }

                
                for (int i = 0; i < flightList.Count; i++)
                {
                    Console.WriteLine(data, voyage[i].flightNumber, voyage[i].type, voyage[i].date,
                        voyage[i].time, voyage[i].cost[0], voyage[i].cost[1], voyage[i].cost[2]);
                }

                Console.WriteLine(line);
                Console.WriteLine();
            }
        }

        public static List<string> GetPlace(Flight[] voyage)
        {
            List<string> punkts = new List<string>();
            for (int i = 0; i < voyage.Length; i++)
            {
                if (punkts.Contains(voyage[i].destination) == false)
                    punkts.Add(voyage[i].destination);
            }
            return punkts;
        }

        // handler of 2 events
        public void ChangeCost(object o)
        {
            Plane plane = (Plane)o;
            cost = plane.cost;
        }

    }
}
