﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace лаба9
{
  
    public struct Flight
    {
        public enum typeList
        {
            Боинг767, Ту154, Ил85, Ту333
        }

        public int number;
        public typeList type;
        public string place;
        public int day;
        public int month;
        public int year;
        public string time;

        public Flight(int pnumber, typeList ptype, string pplace, int pday, int pmonth, int pyear, string ptime)
        {
            type = ptype;
            place = pplace;
            day =pday;
            month=pmonth;
            year = pyear;
            number = pnumber;
            time = ptime;
        }

        public int CountDay
        {
            get
            {
                DateTime d = new DateTime(year, month, day);
                TimeSpan time = d - DateTime.Now;
                return time.Days;
            }
        }

        public void ConclusionData()
        {
            Console.WriteLine("|{0,-17}| {1,2}.{2,2}.{3,4} |{4,-17}|", type, day, month, year, CountDay);
        }
    };
   

    class Program
    {
        static void Main(string[] args)
        {
            
            StreamReader f = new StreamReader("text.txt", Encoding.Default);
            String s = f.ReadToEnd();
            f.Close();
            string s1 = s;


            string[] count = s.Split('\n');
            Flight[] CountString = new Flight[count.Length];
            StringReader str = new StringReader(s);
            for (int i = 0; i < CountString.Length; i++)
            {
                s = str.ReadLine();
                if (s != null)
                {
                    string[] data = s.Split(';');
                    CountString[i] = new Flight(Convert.ToInt32(data[0]),(Flight.typeList)Enum.Parse(typeof(Flight.typeList), data[1]), data[2], Convert.ToInt32(data[3]), Convert.ToInt32(data[4]), Convert.ToInt32(data[5]), data[6]);
                }
            }

            int click = 5;
            while (click != 0)
            {
                Console.Clear();
                Console.WriteLine("\t\t Информация о рейсах по типу самолёта:");
                Console.WriteLine("1. Боинг767");
                Console.WriteLine("2. Ту154");
                Console.WriteLine("3. Ил85");
                Console.WriteLine("4. Ту333");
                Console.WriteLine("0. Выход.");

                Console.WriteLine("сделайте свой выбор");
                int vibor = Convert.ToInt32(Console.ReadLine());

                switch (vibor)
                {

                    case 1:
                        {
                           
                            Console.WriteLine("\t\tВыберите цвет текста:");
                            Console.WriteLine("Insert - Дублирование данных в новый файл.");
                            Console.WriteLine("Другая кнопка - Не дублирует информацию.");
                            ConsoleKeyInfo k = Console.ReadKey();

                             if (k.Key == ConsoleKey.Insert)
                            {
                                StreamWriter file1 = new StreamWriter("Боинг767.txt");

                                Console.Clear();
                                Console.WriteLine("Название самолёта:\n{0}", Flight.typeList.Боинг767);

                                Console.WriteLine("+------------------------------------------------+");
                                Console.WriteLine("|    Номер рейса  |Время вылета|    Кол-во дней  |");
                                Console.WriteLine("+------------------------------------------------+");
                                int p1 = 0;
                                for (int i = 0; i < CountString.Length; ++i)
                                    if (CountString[i].type == Flight.typeList.Боинг767)
                                    {
                                        p1++;
                                        CountString[i].ConclusionData();
                                        Color.output(file1, CountString[i].number, CountString[i].time, CountString[i].CountDay);
                                    }
                                if (p1 == 0) Console.WriteLine("\t\t\tнет данных для ввода");
                                Console.WriteLine("+------------------------------------------------+");
                                file1.Close();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("Название самолёта:\n{0}", Flight.typeList.Боинг767);

                                Console.WriteLine("+------------------------------------------------+");
                                Console.WriteLine("|    Номер рейса  |Время вылета|    Кол-во дней  |");
                                Console.WriteLine("+------------------------------------------------+");
                                int p1 = 0;
                                for (int i = 0; i < CountString.Length; ++i)
                                    if (CountString[i].type == Flight.typeList.Боинг767)
                                    {
                                        p1++;
                                        CountString[i].ConclusionData();
                                        Color.output( CountString[i].number, CountString[i].time, CountString[i].CountDay);
                                    }
                                if (p1 == 0) Console.WriteLine("\t\t\tнет данных для ввода");
                                Console.WriteLine("+------------------------------------------------+");
                            }
                            Console.ReadKey();
                        }
                        break;
                   
                    
                    
                    case 2:
                        {
                            Console.WriteLine("\t\tВыберите цвет текста:");
                            Console.WriteLine("Insert - Дублирование данных в новый файл.");
                            Console.WriteLine("Другая кнопка - Не дублирует информацию.");
                            ConsoleKeyInfo k = Console.ReadKey();

                            if (k.Key == ConsoleKey.Insert)
                            {
                                StreamWriter file1 = new StreamWriter("Ту154.txt");

                                Console.Clear();
                                Console.WriteLine("Название самолёта:\n{0}", Flight.typeList.Ту154);

                                Console.WriteLine("+------------------------------------------------+");
                                Console.WriteLine("|    Номер рейса  |Время вылета|    Кол-во дней  |");
                                Console.WriteLine("+------------------------------------------------+");
                                int p1 = 0;
                                for (int i = 0; i < CountString.Length; ++i)
                                    if (CountString[i].type == Flight.typeList.Ту154)
                                    {
                                        p1++;
                                        CountString[i].ConclusionData();
                                        Color.output(file1, CountString[i].number, CountString[i].time, CountString[i].CountDay);
                                    }
                                if (p1 == 0) Console.WriteLine("\t\t\tнет данных для ввода");
                                Console.WriteLine("+------------------------------------------------+");
                                file1.Close();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("Название самолёта:\n{0}", Flight.typeList.Ту154);

                                Console.WriteLine("+------------------------------------------------+");
                                Console.WriteLine("|    Номер рейса  |Время вылета|    Кол-во дней  |");
                                Console.WriteLine("+------------------------------------------------+");
                                int p1 = 0;
                                for (int i = 0; i < CountString.Length; ++i)
                                    if (CountString[i].type == Flight.typeList.Ту154)
                                    {
                                        p1++;
                                        CountString[i].ConclusionData();
                                        Color.output(CountString[i].number, CountString[i].time, CountString[i].CountDay);
                                    }
                                if (p1 == 0) Console.WriteLine("\t\t\tнет данных для ввода");
                                Console.WriteLine("+------------------------------------------------+");
                            }
                            Console.ReadKey();
                        }
                        break;
                    case 3:
                        {
                            Console.WriteLine("\t\tВыберите цвет текста:");
                            Console.WriteLine("Insert - Дублирование данных в новый файл.");
                            Console.WriteLine("Другая кнопка - Не дублирует информацию.");
                            ConsoleKeyInfo k = Console.ReadKey();

                            if (k.Key == ConsoleKey.Insert)
                            {
                                StreamWriter file1 = new StreamWriter("Ил85.txt");

                                Console.Clear();
                                Console.WriteLine("Название самолёта:\n{0}", Flight.typeList.Ил85);

                                Console.WriteLine("+------------------------------------------------+");
                                Console.WriteLine("|    Номер рейса  |Время вылета|    Кол-во дней  |");
                                Console.WriteLine("+------------------------------------------------+");
                                int p1 = 0;
                                for (int i = 0; i < CountString.Length; ++i)
                                    if (CountString[i].type == Flight.typeList.Ил85)
                                    {
                                        p1++;
                                        CountString[i].ConclusionData();
                                        Color.output(file1, CountString[i].number, CountString[i].time, CountString[i].CountDay);
                                    }
                                if (p1 == 0) Console.WriteLine("\t\t\tнет данных для ввода");
                                Console.WriteLine("+------------------------------------------------+");
                                file1.Close();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("Название самолёта:\n{0}", Flight.typeList.Ил85);
                                Console.WriteLine("+------------------------------------------------+");
                                Console.WriteLine("|    Номер рейса  |Время вылета|    Кол-во дней  |");
                                Console.WriteLine("+------------------------------------------------+");
                                int p1 = 0;
                                for (int i = 0; i < CountString.Length; ++i)
                                    if (CountString[i].type == Flight.typeList.Ил85)
                                    {
                                        p1++;
                                        CountString[i].ConclusionData();
                                        Color.output(CountString[i].number, CountString[i].time, CountString[i].CountDay);
                                    }
                                if (p1 == 0) Console.WriteLine("\t\t\tнет данных для ввода");
                                Console.WriteLine("+------------------------------------------------+");
                            }
                            Console.ReadKey();
                        }
                        break;
                    case 4:
                        {
                            Console.WriteLine("\t\tВыберите цвет текста:");
                            Console.WriteLine("Insert - Дублирование данных в новый файл.");
                            Console.WriteLine("Другая кнопка - Не дублирует информацию.");
                            ConsoleKeyInfo k = Console.ReadKey();

                            if (k.Key == ConsoleKey.Insert)
                            {
                                StreamWriter file1 = new StreamWriter("Ту333.txt");

                                Console.Clear();
                                Console.WriteLine("Название самолёта:\n{0}", Flight.typeList.Ту333);

                                Console.WriteLine("+------------------------------------------------+");
                                Console.WriteLine("|    Номер рейса  |Время вылета|    Кол-во дней  |");
                                Console.WriteLine("+------------------------------------------------+");
                                int p1 = 0;
                                for (int i = 0; i < CountString.Length; ++i)
                                    if (CountString[i].type == Flight.typeList.Ту333)
                                    {
                                        p1++;
                                        CountString[i].ConclusionData();
                                        Color.output(file1, CountString[i].number, CountString[i].time, CountString[i].CountDay);
                                    }
                                if (p1 == 0) Console.WriteLine("\t\t\tнет данных для ввода");
                                Console.WriteLine("+------------------------------------------------+");
                                file1.Close();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("Название самолёта:\n{0}", Flight.typeList.Ту333);

                                Console.WriteLine("+------------------------------------------------+");
                                Console.WriteLine("|    Номер рейса  |Время вылета|    Кол-во дней  |");
                                Console.WriteLine("+------------------------------------------------+");
                                int p1 = 0;
                                for (int i = 0; i < CountString.Length; ++i)
                                    if (CountString[i].type == Flight.typeList.Ту333)
                                    {
                                        p1++;
                                        CountString[i].ConclusionData();
                                        Color.output(CountString[i].number, CountString[i].time, CountString[i].CountDay);
                                    }
                                if (p1 == 0) Console.WriteLine("\t\t\tнет данных для ввода");
                                Console.WriteLine("+------------------------------------------------+");
                            }
                            Console.ReadKey();
                        }
                        break;
                    case 0:
                        {
                            Console.WriteLine("Нажмите любую кнопку для выхода...");
                            click = 0;
                        }
                        break;
                }
            }

            Console.ReadKey();
       }
    }
}
