﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class SumForm1 : Form
    {
        public SumForm1()
        {
            InitializeComponent();
        }

        public string option = "Sum from..to";
        public int a;
        public int b;

        private void SetProperty(bool visible)
        {
            label1.Visible = visible;
            label2.Visible = visible;
            textBox1.Visible = visible;
            textBox2.Visible = visible;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            a = Convert.ToInt32(textBox1.Text);
            b = Convert.ToInt32(textBox2.Text);
            Close();
        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            option = comboBox1.SelectedItem.ToString();
            if (option == "Sum from..to")
            {
                SetProperty(true);
            }
            else if (option == "Sum between negatives")
            {
                SetProperty(false);
            }
        }

    }
}
