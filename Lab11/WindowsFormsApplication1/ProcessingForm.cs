﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class ProcessingForm : Form
    {
        public ProcessingForm()
        {
            InitializeComponent();
        }


        public void PrintMassiv(Array massivObject)
        {
            dataGridView1.RowCount = massivObject.Length;
            dataGridView1.ColumnCount = 1;
            for (int i = 0; i < massivObject.Length; i++)
                dataGridView1[0, i].Value = massivObject[i];

        }
    }
}
