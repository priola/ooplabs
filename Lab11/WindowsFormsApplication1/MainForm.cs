﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        Array massivObject = null;

        private void newToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            richTextBox1.Visible = true;
            dataGridView1.Visible = false;
        }

        private void openToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamReader f = new StreamReader(openFileDialog1.FileName);
                string s = f.ReadToEnd();
                f.Close();
                dataGridView1.Visible = true;
                richTextBox1.Visible = false;

                int[] massiv = null;

                string line = s;
                
                string[] elements = line.Split(';');
                if (massiv == null)
                    massiv = new int[elements.Length];
                for (int j = 0; j < elements.Length; j++)
                    massiv[j] = Convert.ToInt32(elements[j]);

                dataGridView1.RowCount = massiv.Length;
                for (int i = 0; i < massiv.Length; i++)
                    dataGridView1[0,i].Value = massiv[i];

                massivObject = new Array(massiv);
            } 
        }

        private void saveToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamWriter f = new StreamWriter(saveFileDialog1.FileName);
                f.Write(richTextBox1.Text);
                f.Close();
            }
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void sumToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SumForm1 sumform = new SumForm1();
            if (sumform.ShowDialog() == DialogResult.Cancel)
            {
                if (sumform.option == "Sum from..to")
                {
                    int a = sumform.a;
                    int b = sumform.b;
                    int result = massivObject.Sum(a, b);
                    label1.Text = result.ToString();
                }
                else if (sumform.option == "Sum between negatives")
                {
                    int result = massivObject.Sum();
                    label1.Text = result.ToString();
                }
            }
        }

        private void processingToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            massivObject.Array1();
            //открытие формы
            ProcessingForm sortingForm = new ProcessingForm();
            sortingForm.PrintMassiv(massivObject);
            sortingForm.Show();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.Show();
        }
    }
}
