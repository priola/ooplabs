﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1
{
    public class Array
    {
        private int[] mas;
        /// <summary>
        /// cв-во для определения длины массива
        /// </summary>
        public int Length
        {
            get { return mas.Length; }
        }

        /// <summary>
        /// индексатор для доступа к элементам поля массива
        /// </summary>
        /// <param name="i">индекс</param>
        /// <returns></returns>
        public int this[int i]
        {
            get { return mas[i]; }
            set { mas[i] = value; }
        }

        /// <summary>
        /// конструктор с параметрами
        /// </summary>
        public Array(int[] data)
        {
            mas = data;
        }

        /// <summary>
        /// Считает сумму между первым и последним отрицательными числами
        /// </summary>
        /// <returns></returns>
        public int Sum()
        {
            bool f = true;
            int j = 0;
            int p=0;
            int sum=0;
            for (int i = 0; i < mas.Length&&f; i++)
                if (mas[i] < 0)
                {
                    f = false;
                    j = i;
                }
            if (f) throw new Exception("No negatives elements !!!");
            else
            {
                f = true;
                for (int i = mas.Length-1; i >=0 && f; i--)
                    if (mas[i] < 0)
                    {
                        f = false;
                        p = i;
                    }
                for (int i = j; i <= p; i++)
                    sum = sum + mas[i];
            }
            return sum;
        }

        /// <summary>
        /// Считает сумму между заданными индексами
        /// </summary>
        /// <param name="first">первый индекс</param>
        /// <param name="last">второй индекс</param>
        public int Sum(int first, int last)
        {
            int sum = 0;
            if (first > 0 && last < mas.Length)
            {
                for (int i = first; i < last; i++)
                    sum = sum + mas[i];
            }
            else throw new Exception("Error enter!!!");
            return sum;
        }

        /// <summary>
        /// находит минимальное по модулю число
        /// </summary>
        /// <returns>возвращает найденное число</returns>
        public int Minimum()
        { 
            int min=mas[0];

            for (int i = 1; i < mas.Length; i++)
                if (min > Math.Abs(mas[i]))
                    min = Math.Abs(mas[i]);
                return min;
        }

        /// <summary>
        /// Метод обработки массива
        /// </summary>
        public void Array1()
        {
            int j = 0;
            int[] array=new int[mas.Length];
            for (int i = 1; i < mas.Length; i = i + 2)
            {
                array[j] = mas[i];
                j++;
            }
            for (int i = 0; i < mas.Length; i = i + 2)
            {
                array[j] = mas[i];
                j++;
            }
            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = array[i];
            }
        }

    }
}
